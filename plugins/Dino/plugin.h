#ifndef DINOPLUGIN_H
#define DINOPLUGIN_H

#include <QQmlExtensionPlugin>

class DinoPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
