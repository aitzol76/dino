/*
 * Copyright (C) 2018 Wproject - Aitzol Berasategi
 *
 * This file is part of MacBank
 *
 * MacBank is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MacBank is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import "components"

Page {
    id: settingsPage
    visible: false

    header: PageHeader {
        id: settingsHeader
        flickable: settingsPlugin
        title: i18n.tr(headerTitle)
        StyleHints {
            foregroundColor: "#FFFFFF"
            backgroundColor: "#1976D2"
            dividerColor: UbuntuColors.slate
        }

        leadingActionBar.actions: [
            Action {
                visible: true
                iconName: "back"
                text: "Back"
                onTriggered: {
                    pageStack.clear(page3)
                    pageStack.push(page1)
                    headerTitle = "Dino"
                }
            }
        ]

        trailingActionBar.actions: [
            Action {
                iconName: "info"
                text: ""
                onTriggered: {
                    headerTitle = i18n.tr("About")
                    pageStack.push(page4)
                }
            }
        ]

        visible: true
    }

    //Listmodels
    ListModel {
        id: unitsSystemModel
        Component.onCompleted: initialise()
          function initialise() {
              unitsSystemModel.append({ "unitsSystem": i18n.tr("Imperial"), index: 0})
              unitsSystemModel.append({ "unitsSystem": i18n.tr("Metric"), index: 1 })
              //unitsSystemSelection.subText.text = settings.unitsSystem
              unitsSystemSelection.subText.text = unitsSystemModel.get(settings.unitsSystem).unitsSystem
          }
    }

    Flickable {
        id: settingsPlugin

        anchors.fill: parent
        contentHeight: _settingsColumn.height

        Column {
            id: _settingsColumn

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            ListItemHeader {
                 id: unitsListHeader
                 title: i18n.tr("Units")
                 dark: true
            }

            ExpandableListItem {
                id: unitsSystemSelection
                listViewHeight: units.gu(13)
                titleText.text: i18n.tr("System")
                //subText.text: settings.unitsSystem
                subText.textSize: Label.Medium

                model: unitsSystemModel

                delegate: ListItem {
                    divider.visible: false
                    ListItemLayout {
                        title.text: model.unitsSystem

                        Icon {
                            SlotsLayout.position: SlotsLayout.Trailing
                            width: units.gu(2)
                            height: width
                            name: "tick"
                            visible: settings.unitsSystem === model.index
                            asynchronous: true
                        }
                    }
                    onClicked: {
                    console.log(unitsSystemModel)
                        //settings.unitsSystem = model.unitsSystem
                        settings.unitsSystem = model.index
                        unitsSystemSelection.subText.text = model.unitsSystem
                        unitsSystemSelection.expansion.expanded = false
                    }
                }
            }

        }
    }

}
