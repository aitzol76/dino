import QtQuick 2.4
import QtQuick.Window 2.2
import Ubuntu.Components 1.3

import QtGraphicalEffects 1.0

PageStack {
    id: introPageStack

    Component.onCompleted: {
        introPageStack.push(pageIntro1)
    }

    LinearGradient {
        anchors.fill: parent
        start: Qt.point(0, 0)
        end: Qt.point(0, parent.height)
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#CCCCCC" }
            GradientStop { position: 1.0; color: "#ECECEC" }
        }
    }

    property var dotColor : ["#fe007e","grey","grey","grey","grey"]

    Page {
        id: pageIntro1
        visible: false
        header: PageHeader {
            visible: false
        }
        Intro1 {}
    }

    Page {
        id: pageIntro2
        visible: false
        header: PageHeader {
            visible: false
        }
        Intro2 {}
    }

    Page {
        id: pageIntro3
        header: PageHeader {
            visible: false
        }
        visible: false
        Intro3 {}
    }
    Page {
        id: pageIntro4
        header: PageHeader {
            visible: false
        }
        visible: false
        Intro4 {}
    }
    Page {
        id: pageIntro5
        header: PageHeader {
            visible: false
        }
        visible: false
        Intro5 {}
    }
}
