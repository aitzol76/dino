import QtQuick 2.4
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import "../components"

Flickable {
    id: intro4
    width: parent.width; height: parent.height

    Image {
        anchors {
            top: parent.top
            bottom: introTitle.top
            bottomMargin: units.gu(6)
            horizontalCenter: parent.horizontalCenter
        }
        source:"../images/dino/enjoy.png"
        scale: density <= 10 ? 0.8 : 1.0

        fillMode: Image.PreserveAspectFit
        antialiasing: true
    }

    Label {
        id: introTitle
        text: i18n.tr("Enjoy!")
        font.pointSize: units.dp(18)
        font.weight: Font.Normal
        height: contentHeight
        anchors.centerIn: parent
        anchors.verticalCenterOffset: density <= 10 ? units.gu(-6) : 0
        color: UbuntuColors.darkGrey

    }

    Label {
        id: bodyText
        text: i18n.tr("I hope you'll like Dino")
        font.pointSize: units.dp(10.5)
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: units.gu(1)
        anchors.top: introTitle.bottom
        anchors.topMargin: units.gu(4)
        anchors.bottom: dotContainer.top
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
    }

    Dots {
        id:dotContainer
        anchors.margins: units.gu(1)
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: units.gu(3)
        scale: density <= 10 ? 0.8 : 1.0
    }
    //---------------Swipe based on MouseArea for desktop -------------------
    MouseArea{
        //enabled if device doesn't have a touch screen
        enabled: deviceType === true ? false:true
        anchors.fill: parent
        property point origin
        property point destination
        //property bool ready: false
        signal move(int x, int y)
        signal swipe(string direction)
        onPressed: {
            drag.axis = Drag.XAxis
            origin = Qt.point(mouse.x, mouse.y)
            console.log(origin)
        }
        onReleased: {
            destination =  Qt.point(mouse.x, mouse.y)
            console.log(destination.x)
            if(origin.x != destination.x){
                if(origin.x < destination.x){

                    console.log("SW-right")
                    dotColor = ["grey", "grey", "grey", "#fe007e", "grey"]
                    introPageStack.push(pageIntro4)

                }else{

                    console.log("SW-left")
                    settings.firstRun = false
                    pageStack.push(page1)

                }

            }

        }
    }
    //-----------------Swipe based on SwipeArea for touchscreen devices ----------------------
    SwipeArea {
        anchors.fill: parent
        direction: SwipeArea.Leftwards
        height: units.gu(50)
        //immediateRecognition : true
        onDraggingChanged:  {
            if ( dragging ) {
                console.log("intro finished")
                settings.firstRun = false
                pageStack.push(page1)
            }
        }
    }
    SwipeArea {
        anchors.fill: parent
        direction: SwipeArea.Rightwards
        height: units.gu(50)
        //immediateRecognition : true
        onDraggingChanged:  {
            if ( dragging ) {
                console.log("SW-right")
                dotColor = ["grey", "grey", "grey", "#fe007e", "grey"]
                introPageStack.push(pageIntro4)
            }
        }
    }
}
