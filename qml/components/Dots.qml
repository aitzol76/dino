import QtQuick 2.4

Row{
    id:dots
    spacing:14
    Rectangle {
         id: dot1
         width: units.gu(2)
         height: width
         color: dotColor[0]
         border.color: "white"
         border.width: 1
         radius: width*0.5
    }

    Rectangle {
         id: dot2
         width: units.gu(2)
         height: width
         color: dotColor[1]
         border.color: "white"
         border.width: 1
         radius: width*0.5
    }

    Rectangle {
         id:dot3
         width: units.gu(2)
         height: width
         color: dotColor[2]
         border.color: "white"
         border.width: 1
         radius: width*0.5
    }

    Rectangle {
         id:dot4
         width: units.gu(2)
         height: width
         color: dotColor[3]
         border.color: "white"
         border.width: 1
         radius: width*0.5
    }

    Rectangle {
         id:dot5
         width: units.gu(2)
         height: width
         color: dotColor[4]
         border.color: "white"
         border.width: 1
         radius: width*0.5
    }
}
