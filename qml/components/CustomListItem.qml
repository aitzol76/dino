/*
 * Copyright (C) 2018 Wproject - Aitzol Berasategi
 *
 * This file is part of MacBank
 *
 * MacBank is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MacBank is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

ListItem {
    id: customListItem

    property var name: ""
    property var content: ""
    property var icon: ""
    property var iconSource: ""
    property var source: iconSource != "" ? listIcon.source = iconSource : {}
    property var iconColor: UbuntuColors.slate
    property var iconWidth: units.gu(4)
    property var iconHeight: units.gu(4)
    property var dividerColor
    property bool progressSymbol
    height: layout.height + (divider.visible ? divider.height : 0)
    divider.colorFrom: dividerColor ? dividerColor : divider.colorFrom

    ListItemLayout {
        id: layout
        title.text: name

        Label{
            text: content
            color: UbuntuColors.ash
        }

        Icon {
            id:listIcon
            name: icon
            color: iconColor
            width: iconWidth
            height: iconHeight
            SlotsLayout.position: SlotsLayout.Leading
            visible: icon != "" || iconSource != "" ? true : false
        }

        ProgressionSlot {
            visible: progressSymbol ? true : false
            SlotsLayout.position: SlotsLayout.Trailing
        }
    }

}
