import QtQuick 2.4
import Ubuntu.Components 1.3

PageHeader {
    id: mainHeader

    title: i18n.tr(headerTitle)
    StyleHints {
        foregroundColor: "#FFFFFF"
        backgroundColor: "#1976D2"
        dividerColor: UbuntuColors.slate
    }

    leadingActionBar.actions: [
        Action {
            visible: false
            iconName: "back"
        }
    ]

    trailingActionBar.actions: [
        Action {
            iconName: "filter"
            text: ""
            onTriggered: {
                expanded = true;
            }
        },
        Action {
            iconName: "settings"
            text: ""
            onTriggered: {
                headerTitle = i18n.tr("Settings")
                pageStack.push(page3)
            }
        },
        Action {
            iconName: "search"
            text: ""
            onTriggered: {
              page1.header = searchHeader
              searchHeader.field.item.forceActiveFocus() //force focus on the search field
            }
        }
    ]

    //trailingActionBar.numberOfSlots: 2
    visible: true
}
