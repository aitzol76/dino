/*
 * Copyright (C) 2018 Wproject - Aitzol Berasategi
 *
 * This file is part of MacBank
 *
 * MacBank is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MacBank is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3

ListItem {
    id: headerListItem

    property string title
    property bool dark

    height: units.gu(4)
    Label {
        anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: units.gu(2) }
        text: title
        font.bold: false
        color: dark ? UbuntuColors.jet : UbuntuColors.ash
    }
}
