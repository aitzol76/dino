import QtQuick 2.4
import Ubuntu.Components 1.3
import "../dino.js" as DinosaursData

PageHeader {
    id: searchHeader

    property var field: searchField //reference to the search field

    StyleHints {
        foregroundColor: "#FFFFFF"
        backgroundColor: "#1976D2"
        dividerColor: UbuntuColors.slate
    }

    contents: Loader {
        id: searchField
        sourceComponent: page1.header === searchHeader ? searchFieldComponent : undefined
        anchors.left: parent ? parent.left : undefined
        anchors.right: parent ? parent.right : undefined
        anchors.verticalCenter: parent.verticalCenter
    }

    leadingActionBar.actions: [
        Action {
            iconName: "back"
            onTriggered: {
                collection = DinosaursData.dinosaurs
                page1.header = mainHeader
            }
        }
    ]

    visible: true
}
