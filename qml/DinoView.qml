import QtQuick 2.4
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import QtGraphicalEffects 1.0



Page {
    id: dinoViewPage
    visible: false

    header: PageHeader {
        id: dinoViewHeader
        title: i18n.tr(headerTitle)
        StyleHints {
            foregroundColor: "#FFFFFF"
            backgroundColor: "#1976D2"
            dividerColor: UbuntuColors.slate
        }

        leadingActionBar.actions: [
            Action {
                visible: true
                iconName: "back"
                text: "Back"
                onTriggered: {
                    pageStack.clear(page2)
                    pageStack.push(page1)
                    page1.header === searchHeader ? searchHeader.field.item.forceActiveFocus() : null //force focus on the search field
                    headerTitle = "Dino"
                }
            }
        ]
        visible: true
    }

    function imgSource(){
            var imgId = collection[selectedIndex].id;
            var source = "images/"+imgFolder+"/img"+imgId+".png"
            //console.log("screen="+orientation)
            //console.log("width:"+width)
            //console.log("height:"+height)
            //console.log(orientation)
            //console.log("width:"+width)
            //console.log("height:"+height)
            console.log(density)
            return source;
    }

    function height_data(){
        var hData = collection[selectedIndex].height;
        var res;
        for (var i=0;i<hData.length;i++){

            function conversion(i){
                var val = hData[i]; //default value in meters
                var u;
                if(settings.unitsSystem == 1 ){ //if Metric

                    if(hData[i] <= 1){ //centimeters
                        val = Math.round((val*100)*10)/10;
                        u = "cm"
                    }else{ //meters
                        val = Math.round(val*10)/10;
                        u = 'm'
                    }

                }else if(settings.unitsSystem == 0 ){ //if Imperial

                    var u = 'ft'
                    if(hData[i] <= 1){ //inches
                        val = Math.round(val*(100*0.393700787401575)*10)/10;
                        u = 'in'
                    }else{ //feets
                        val = Math.round(val/0.3048);
                    }
                }

                return(val.toString() + " " + u);
            }

            if(res == undefined){
                if(isNaN(hData[i])){ //not a number
                    res = i18n.tr(hData[i]);
                }else{
                    res = conversion(i);
                }
            }else{
                if(isNaN(hData[i])){ //not a number
                    function addStr(i){
                        var str = " (" + i18n.tr(hData[i]) + ")";
                        if((i+1) < hData.length){
                            str = str + ";"
                        }
                        return(str);
                    }
                    res = res + addStr(i)
                }else{
                    if(res.endsWith(';')){
                        res = res + " " + conversion(i)
                    }else{
                        res = res + " - " + conversion(i)
                    }
                }
            }
        }
        return(res)
    }

    function weight_data(){
        var wData = collection[selectedIndex].weight;
        console.log(wData)
        var res;
        for (var i=0;i<wData.length;i++){

            function conversion(i){
                var val = wData[i]; //default value in grams
                var u;

                if(settings.unitsSystem == 1 ){ //if Metric
                    if(wData[i] <= 1000){ //grams
                        val = Math.round(val*10)/10;
                        u = 'gr'
                    }else if((wData[i] >= 1001) && (wData[i] <= 999999)){ //kilograms
                        val = Math.round((val/1000)*10)/10;
                        u = 'Kg'
                    }else if(wData[i] >= 1000000){ //tonnes
                        val = Math.round((val/1000000)*10)/10
                        u = 'Mg'
                    }

                }else if(settings.unitsSystem == 0 ){ //if Imperial

                    if(wData[i] <= 499){ //ounces
                        val = Math.round(val*0.035274*10)/10;
                        u = 'oz'
                    }else if((wData[i] >= 500) && (wData[i] <= 999999)){ //pounds
                        val = Math.round(val*0.0022046);
                        u = 'lb'
                    }else if(wData[i] >= 1000000){
                        val = Math.round(val*0.0000011023);
                        u = 't'
                    }
                }

                return(val.toString() + " " + u);
            }

            if(res == undefined){
                if(isNaN(wData[i])){ //not a number
                    res = i18n.tr(wData[i]);
                }else{
                    res = conversion(i);
                }
            }else{
                if(isNaN(wData[i])){ //not a number
                    function addStr(i){
                        var str = " (" + i18n.tr(wData[i]) + ")";
                        if((i+1) < wData.length){
                            str = str + ";"
                        }
                        return(str);
                    }
                    res = res + addStr(i)
                }else{
                    if(res.endsWith(';')){
                        res = res + " " + conversion(i)
                    }else{
                        res = res + " - " + conversion(i)
                    }
                }
            }
        }
        return(res)
    }

    Flickable {
        id: dinoContainer
        width: parent.width; height: parent.height
        anchors.top: dinoViewHeader.bottom
        anchors.bottom: parent.bottom

        ScrollView {
            id: dataCont
            width: orientation === 1 ? parent.width : parent.width/2
            height: orientation === 1 ? parent.height/2 : parent.height
            anchors.left : orientation === 1 ? parent.left : parent.left

            Column {
                width: orientation === 1 ? dinoContainer.width : dinoContainer.width/2

                ListItem.Standard {
                    text: "<b>"+  i18n.tr("Meaning")+"</b>: "  + i18n.tr(collection[selectedIndex].meaning)
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Period")+"</b>: " + i18n.tr(collection[selectedIndex].period)
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Diet")+"</b>: " + i18n.tr(collection[selectedIndex].diet)
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Height")+"</b>: " + height_data()
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Weight")+"</b>: " + weight_data()
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Locomotion")+"</b>: " + i18n.tr(collection[selectedIndex].locomotion)
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Species")+"</b>: " + i18n.tr(collection[selectedIndex].species)
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Family")+"</b>: " + collection[selectedIndex].family
                }
                ListItem.Standard {
                    text: "<b>"+ i18n.tr("Home")+"</b>: " + i18n.tr(collection[selectedIndex].home)
                }
                ListItem.Caption {
                    id:description
                    text: "<b>"+ i18n.tr("Description")+"</b>: " + i18n.tr(collection[selectedIndex].description)
                }

                ListItem.ThinDivider{
                    height: 2
                }
            }

        }

        Rectangle{
            id: imgCont
            width: orientation === 2 ? parent.width/2 : parent.width
            height: orientation === 2 ? parent.height/2 : parent.height*2
            anchors.top: orientation === 1 ? dataCont.bottom : parent.top
            anchors.right:parent.right
            anchors.bottom:parent.bottom

            gradient: Gradient
            {
                GradientStop { position: 0.5; color: "#ffffff" }
                GradientStop { position: 1.0; color: "#ebebe0" }
            }

            Rectangle {
                id: verticalDivider
                border.width: .5
                height: parent.height
                width: 2
                anchors.left: parent.left
                border.color: "#cccccc"
                visible: orientation === 2 ? true : false
            }

            Image {
                id:dinoImage
                anchors.fill: parent
                anchors.margins: parent.width/20
                anchors.verticalCenter: parent.verticalCenter
                //anchors.horizontalCenter: parent.horizontalCenter
                anchors.horizontalCenter: orientation === 1 ? parent.horizontalCenter : imgCont.horizontalCenter
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
                source:imgSource()
                fillMode: Image.PreserveAspectFit
                smooth: true
                visible: false

            }
            DropShadow {
                anchors.fill: dinoImage
                horizontalOffset: 0
                verticalOffset: 20
                radius: 8.0
                samples: 17
                color: "#999966"
                source: dinoImage
            }

        }

        //---------------Swipe based on MouseArea for desktop -------------------
        MouseArea{
            //enabled if device doesn't have a touch screen
            enabled: deviceType === true ? false:true
            anchors.fill: parent
            property point origin
            property point destination
            //property bool ready: false
            signal move(int x, int y)
            signal swipe(string direction)
            onPressed: {
                drag.axis = Drag.XAxis
                origin = Qt.point(mouse.x, mouse.y)
                console.log(origin)
            }
            onReleased: {
                destination =  Qt.point(mouse.x, mouse.y)
                console.log(destination.x)
                if(origin.x != destination.x){
                    if(origin.x < destination.x){

                        if(selectedIndex != 0){
                            selectedIndex = selectedIndex - 1
                            console.log("SW-right")
                            console.log(selectedIndex)
                            headerTitle = collection[selectedIndex].name
                            pageStack.push(page2)
                        }

                    }else{

                        if(selectedIndex < items){
                            selectedIndex = selectedIndex + 1
                            console.log("SW-left")
                            console.log(selectedIndex)
                            headerTitle = collection[selectedIndex].name
                            pageStack.push(page2)
                        }

                    }

                }

            }
        }
        //-----------------Swipe based on SwipeArea for touchscreen devices ----------------------
        SwipeArea {
            //enabled if device has a touch screen
            enabled: deviceType === true ? true:false
            anchors.fill: parent
            direction: SwipeArea.Leftwards
            height: units.gu(50)
            //immediateRecognition : true
            onDraggingChanged:  {
                if ( dragging ) {
                    if(selectedIndex < items){
                        selectedIndex = selectedIndex + 1
                        console.log("SW-left")
                        console.log(selectedIndex)
                        headerTitle = collection[selectedIndex].name
                        pageStack.push(page2)
                    }
                }
            }
        }
        SwipeArea {
            anchors.fill: parent
            direction: SwipeArea.Rightwards
            height: units.gu(50)
            //immediateRecognition : true
            onDraggingChanged:  {
                if ( dragging ) {
                    if(selectedIndex != 0){
                        selectedIndex = selectedIndex - 1
                        console.log("SW-right")
                        console.log(selectedIndex)
                        headerTitle = collection[selectedIndex].name
                        pageStack.push(page2)
                    }
                }
            }
        }

    //--------------------------

    }

}
